// v.1.2.3
// v.1.3
// v.1.3.1
// v.1.3.2
// v.1.3.3
// v.1.3.4
// v.1.3.5 input time

class iSetup {
    constructor(opt = {}) {
        this.modal = $("#" + opt.modal);
        this.form = this.modal.find('form')[0];
        this.urldetail = opt.urldetail;
        this.urlsave = opt.urlsave;
        this.saveparam = opt.saveparam;
        this.savesuccess = opt.savesuccess;
        this.detailsuccess = opt.detailsuccess;
        this.setupready = opt.setupready;
        this.setupclose = opt.setupclose;
        this.onopenmodalcreate = opt.onopenmodalcreate;
        this.beforesubmit = opt.beforesubmit;

        this.loading = false;
        let t = this;

        t.modal.on('shown.bs.modal', function (e) {
            if (typeof t.setupready === 'function') t.setupready();
        });

        t.modal.on('hidden.bs.modal', function (e) {
            if (typeof t.setupclose === 'function') t.setupclose();
        });

        $(this.form).on('submit', function (event) {
            event.preventDefault();
            if (t.loading) return;
            if (t.beforesubmit) {
                if (t.beforesubmit(event) === false) {
                    return;
                }
            }
            $.ajax({
                url: t.urlsave,
                type: 'POST',
                data: t.saveparam(),
                success: function (r) { loading(false); t.savesuccess(r); },
                error: function (xhr) { loading(false); alerterror(xhr); },
                beforeSend: function () { t.loading = true; loading(true); },
                complete: function () { t.loading = false; }
            });
        });
    }

    opensetup(proc, param) {
        let t = this;
        t.form['_PROCESS'].value = proc;
        if (proc === 'EDIT' || proc === 'CREATE') {
            t.modal.find('.btn-delete').addClass('hide');
            t.modal.find('.btn-save').removeClass('hide');
        } else if (proc === 'VIEW') {
            t.modal.find('.btn-save').addClass('hide');
            t.modal.find('.btn-delete').addClass('hide');
        } else {
            t.modal.find('.btn-save').addClass('hide');
            t.modal.find('.btn-delete').removeClass('hide');
        }
        if (proc === "CREATE") {
            t.form.reset();
            $(t.form).find('[type="hidden"]').val('');
            t.form['_PROCESS'].value = proc;
            t.init();
            if (t.onopenmodalcreate !== undefined) t.onopenmodalcreate(t.form, param);
            t.modal.modal({ backdrop: 'static' });
        } else if (proc === "EDIT" || proc === "DELETE" || proc === "VIEW") {
            $.ajax({
                url: t.urldetail,
                type: 'POST',
                data: param,
                success: function (r) { t.detailsuccess(r, proc); },
                error: function (xhr) { alerterror(xhr); },
                beforeSend: function () { loading(true); },
                complete: function () { loading(false); }
            });
        }
    }

    init() {
        //all-readonly
        //all-disabled
        //all-hide

        //CREATE, EDIT
        //setup-readonly
        //setup-disabled
        //setup-hide

        //VIEW, DELETE
        //view-disabled
        //view-hide
        let f = this.form;
        let inputreadonly = 'input[type="text"],input[type="number"],input[type="time"],input[type="date"],[type="email"],[type="password"],textarea';
        let inputdisabled = 'select,input[type="checkbox"],input[type="radio"]';
        if (f['_PROCESS'].value === 'CREATE' || f['_PROCESS'].value === 'EDIT') {
            $(f).find(inputreadonly).removeAttr('readonly');
            $(f).find(inputdisabled).removeAttr('disabled');

            $(f).find('.view-disabled').removeAttr('disabled');
            $(f).find('.setup-readonly').attr('readonly', 'readonly');
            $(f).find('.setup-disabled').attr('disabled', 'disabled');
            $(f).find('.setup-hide').addClass('hide');
            $(f).find('.view-hide').removeClass('hide');

        } else if (f['_PROCESS'].value === 'VIEW' || f['_PROCESS'].value === 'DELETE') {
            $(f).find(inputreadonly).attr('readonly', 'readonly');
            $(f).find(inputdisabled).attr('disabled', 'disabled');

            $(f).find('.view-disabled').attr('disabled', 'disabled');
            $(f).find('.view-hide').addClass('hide');
            $(f).find('.setup-hide').removeClass('hide');
        }
        if (f['_PROCESS'].value === 'CREATE')
        {
            $(f).find('.create-clear').val('');
        }
        $(f).find('.all-readonly').attr('readonly', 'readonly');
        $(f).find('.all-disabled').attr('disabled', 'disabled');
        $(f).find('select').trigger('change');
    }
}

class iTable {
    constructor(opt = {}) {
        this.table = opt.table;
        this.property(opt);
        this.onloading = false;
        this.url = opt.url;
        this.tablesuccess = opt.tablesuccess;
        this.tablebeforeSend = opt.tablebeforeSend;
        this.filter = opt.filter;
        let t = this;

        $(this.table).find('[data-itable-sorting]').on('click', function () {
            let _orderby = $(this).data('itable-sorting');
            let _orderbytype = _orderby === t.orderby ? !t.orderbytype : true;
            t.property({ orderby: _orderby, orderbytype: _orderbytype });
            t.pageindex = 0;
            t.refresh();
        });

        $(this.table).on('submit', function (event) {
            event.preventDefault();
            t.refresh();
        });
    }

    refresh() {
        if (this.onloading) return;
        this.pagesize = parseInt($(this.table).find('.itable-pagesize option:selected').val() || 0);
        let _filter = [];
        if (this.filter) {
            _filter = this.filter();
        } else {
            $.each($(this.table).find('[data-itable-filter]'), function (index, value) {
                _filter.push({
                    Key: $(value).data('itable-filter'),
                    Type: $(value).data('itable-filter-type'),
                    Value: value.type === 'checkbox' ? value.checked : value.value
                });
            });
        }
        let t = this;
        $(this.table).find('tbody').html('');
        $.ajax({
            url: this.url,
            type: 'POST',
            data: {
                orderby: this.orderby,
                orderbytype: this.orderbytype,
                pagesize: this.pagesize,
                pageindex: this.pageindex,
                filter: _filter
            },
            success: function (r) {
                r = JSON.parse(r);
                t.tablesuccess(r, t);
                if (r.IsSuccess) {
                    $(t.table).find('[data-itable-page]').on('click', function () {
                        t.property({ pageindex: $(this).data('itable-page') });
                        t.refresh();
                    });
                }
            },
            error: function (xhr) { alerterror(xhr); },
            beforeSend: function () { t.onloading = true; loading(true); if (t.tablebeforeSend === 'function') { t.tablebeforeSend(); } },
            complete: function () { t.onloading = false; loading(false); }
        });
    }

    property(opt = {}) {
        let t = this;
        t.orderby = opt.orderby || t.orderby;
        let _orderbytype = t.orderbytype === undefined ? true : t.orderbytype;
        t.orderbytype = opt.orderbytype === undefined ? _orderbytype : opt.orderbytype;
        let _pageindex = t.pageindex === undefined ? 0 : t.pageindex;
        t.pageindex = opt.pageindex === undefined ? _pageindex : opt.pageindex;
        $(t.table).find('[data-itable-sorting]').removeClass('sorting').removeClass('sorting_asc').removeClass('sorting_desc');
        $.each($(t.table).find('[data-itable-sorting]'), function (index, value) {
            if ($(value).data('itable-sorting') === t.orderby)
                $(value).addClass('sorting_' + (t.orderbytype ? 'asc' : 'desc'));
            else
                $(value).addClass('sorting');
        });
    }
}

const generatefootertable = (totalcount, pagesize, currentpage) => {
    let startdata;
    if (totalcount === 0) startdata = 0;
    else startdata = currentpage * pagesize + 1;
    let maxpage = Math.ceil(totalcount / pagesize);
    let enddata = startdata + pagesize - 1;
    if (enddata > totalcount) enddata = totalcount;
    let datainfo = 'Data ' + startdata + '-' + enddata + ' of ' + totalcount + ' Page (' + (totalcount === 0 ? 0 : currentpage + 1) + '/' + maxpage + ')';
    let pagination = "";
    if (totalcount > 0) {
        let start_page = currentpage > 0 ? currentpage - 1 : 0;
        let end_page = currentpage + 1;
        if (currentpage === 0) end_page++;
        end_page = end_page > maxpage - 1 ? maxpage - 1 : end_page;
        if (currentpage >= end_page && start_page > 0) start_page--;
        let disable = ' disabled"';
        let string_page = '<a' + (currentpage === 0 ? '' : ' data-itable-page="0"') + ' class="paginate_button previous' + (currentpage === 0 ? disable : '') + '">&laquo;</a>';
        string_page += '<a' + (currentpage === 0 ? '' : ' data-itable-page="' + (currentpage - 1) + '"') + ' class="paginate_button previous' + (currentpage === 0 ? disable : '') + '">&lsaquo;</a>';
        string_page += '<span>';
        if (start_page > 0)
            string_page += '<a class="paginate_button" data-itable-page="' + (start_page - 1) + '">..</a>';
        for (let i = start_page; i <= end_page; i++) {
            let active = i === currentpage;
            string_page += '<a class="paginate_button' + (active ? ' current' : '') + '" ' + (active ? '' : ' data-itable-page="' + i + '"') + '>' + (i + 1) + '</a>';
        }
        if (end_page + 1 < maxpage)
            string_page += '<a class="paginate_button" data-itable-page="' + (end_page + 1) + '">..</a>';
        string_page += '</span>';
        string_page += '<a' + (currentpage + 1 === maxpage ? '' : ' data-itable-page="' + (currentpage + 1) + '"') + ' class="paginate_button next' + (currentpage + 1 === maxpage ? disable : '') + '">&rsaquo;</a>';
        string_page += '<a' + (currentpage + 1 === maxpage ? '' : ' data-itable-page="' + (maxpage - 1) + '"') + ' class="paginate_button next' + (currentpage + 1 === maxpage ? disable : '') + '">&raquo;</a>';
        pagination = string_page;
    }
    return {
        datainfo: datainfo,
        pagination: pagination
    };
}

const loading = open => {
    if (open) $('#loading').removeClass('hide');
    else $('#loading').addClass('hide');
}

const removedetail = t => {
    $(t).closest('tr').remove();
}

const alerterror = xhr => {
    if (xhr.responseText !== undefined) {
        if (xhr.responseText.split('<title>').length > 0) {
            alert(xhr.responseText.split('<title>')[1].split('</title>')[0]);
        } else
            alert(xhr.responseText);
    } else if (xhr.statusText !== undefined) {
        alert(xhr.statusText);
    } else {
        alert(xhr);
    }
}

const getcurrencyfromfloat = text => {
    if (text === '0') return '0';
    if (text === '') return '';
    if (text === '-0') return '-';
    if (!text) return '';
    text = text.toString().replace(/\,/g, '');
    let point = text.split('.')[1] || '';
    let number = parseFloat(text.toString().replace(/\./g, '.'));
    let arrResult = number.toString().replace(/\,/g, '.').split('.');
    let result = arrResult[0].substr(0, 16).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    result = result === "NaN" ? "" : result;
    if (arrResult.length > 1) return result + '.' + point;
    else return result;
}

const getfloatfromcurrency = currency => {
    if (!currency) { return ""; }
    if (currency === "-") currency = "-0";
    return parseFloat(currency.toString().replace(/[^0-9\.-]+/g, ""));
}

const formatdate = (date, format) => {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    if (format === "dd/MM/yyyy")
        return [day, month, year].join('/');
    else
        return [year, month, day].join('-');
}

const setCookie = (name, value, days) => {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

const getCookie = name  => {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

const eraseCookie = name => {
    document.cookie = name + '=; Max-Age=-99999999;';
}

const sidebartoggleclick = () => {
    setCookie('sidebar', $('body').hasClass('sidebar-xs').toString(), 360);
}

const initmahas = () => {
    let li = $('.sidebar.sidebar-main li a[href="' + window.location.pathname + '"]').parent('li');
    li.addClass('active');
    let ul = li.parent('ul:not(.navigation)');
    if (ul.length > 0) {
        ul.css('display', 'block');
        ul.closest('li').addClass('active');
    }
}