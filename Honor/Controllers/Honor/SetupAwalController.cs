﻿using Honor.Entities.SIM;
using Honor.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Honor.Models.Honor;
using System.Net.Configuration;

namespace Honor.Controllers.Honor
{
    [Authorize]
    public class SetupAwalController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var model = new SetupAwalModel();
                    foreach (var x in s.HR_GetSetupAwal().ToList())
                    {
                        model = new SetupAwalModel()
                        {
                            NPWP = x.NPWP,
                            Nilai = x.Persen.ToString()
                        };
                    }
                    return View(model);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(SetupAwalModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        id = s.HR_SetupAwal(model.Nilai, model.NPWP);
                        s.SaveChanges();

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupAwal;".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        //[HttpPost]
        //public string Detail(string id)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var m = s.ADM_GetJasaTidakHonor.FirstOrDefault(x => x.JasaID == id);
        //            if (m == null) throw new Exception("Data tidak ditemukan");
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = new
        //                {
        //                    JasaID = m.JasaID,
        //                    Opt = m.Opt,
        //                    TipeHonor = m.TipeHonor,
        //                    TipePasien = m.TipePasien,
        //                    JasaName = m.JasaName,


        //                }
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}

        #endregion
    }
}