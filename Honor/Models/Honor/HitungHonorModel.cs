﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honor.Models.Honor
{
    public class HitungHonorModel
    {
        public string NoBukti { get; set; }
        public string TipeHonor { get; set; }
        public DateTime Periode { get; set; }
        public string FilterDokter { get; set; }
        public string Keterangan { get; set; }
    }

    public class HitungHonorDetailPasienModel 
    {
        public string NoBukti { get; set; }
        public string NoInvoice { get; set; }
    }
}