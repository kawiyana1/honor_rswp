﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Honor.Startup))]
namespace Honor
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
